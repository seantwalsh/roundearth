<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Class FirstValue.
 *
 * Return the first value that is not empty.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_first_value"
 * )
 */
class FirstValue extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Iterate over the items, return the first non-empty item.
    foreach ($value as $item) {
      if (!empty($item)) {
        return $item;
      }
    }

    // Or NULL if none present.
    return NULL;
  }

  public function multiple() {
    return TRUE;
  }

}
