<?php
/**
 * @file
 * Theme and preprocess functions for nodes
 */

/**
 * Implements hook_preprocess_node().
 */
function pangea_preprocess_node(&$vars) {
  if ($vars['view_mode'] == 'teaser') {
    $vars['attributes']['class'][] = 'clearfix';
  }
}

