<?php

namespace Drupal\roundearth_events\Menu;

use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Local task plugin class for CiviCRM event.
 */
class EventsCivicrmLocalTask extends LocalTaskDefault {

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    $raw_variables = $route_match->getRawParameters();
    $parameters = [
      'reset' => '1',
      'action' => 'update',
    ];

    if ($raw_variables->has('civicrm_event')) {
      $parameters['id'] = $raw_variables->get('civicrm_event');
    }
    else {
      $parameters['id'] = $_GET['id'];
    }

    return $parameters;
  }


}