<?php

namespace Drupal\roundearth_migration\Plugin\migrate\source\d6;

use Drupal\comment\Plugin\migrate\source\d6\Comment as CommentD6;

/**
 * Custom D6 node source.
 *
 * Replaces the class for the 'd6_node' plugin.
 */
class Comment extends CommentD6 {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    if (!empty($this->configuration['node_type'])) {
      $node_type = $this->configuration['node_type'];
      if (!is_array($node_type)) {
        $node_type = [ $node_type ];
      }
      $query->condition('n.type', $node_type, 'IN');
    }
    return $query;
  }

}