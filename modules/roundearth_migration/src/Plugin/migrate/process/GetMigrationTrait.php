<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\Plugin\MigrationInterface;

trait GetMigrationTrait {

  /**
   * Uses the 'migration_lookup' process plugin to migrated IDs.
   *
   * @param mixed $value
   *   The value to be transformed.
   * @param array $configuration
   *   An array of configuration relevant to the plugin instance.
   * @param \Drupal\migrate\MigrateExecutableInterface $migrate_executable
   *   The migration in which this process is being executed.
   * @param \Drupal\migrate\Row $row
   *   The row from the source to process. Normally, just transforming the value
   *   is adequate but very rarely you might need to change two columns at the
   *   same time or something like that.
   * @param string $destination_property
   *   The destination property currently worked on. This is only used together
   *   with the $row above.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration in use.
   *
   * @return mixed|null
   *   The migrated IDs.
   *
   * @see \Drupal\migrate\Plugin\MigrateProcessInterface::transform.
   */
  protected function getDestId($value, array $configuration, $migrate_executable, $row, $destination_property, MigrationInterface $migration) {
    /** @var \Drupal\migrate\Plugin\MigratePluginManager $manager */
    $manager = \Drupal::service('plugin.manager.migrate.process');
    if (!array_key_exists('source', $configuration)) {
      $configuration['source'] = $value;
    }
    if (!array_key_exists('no_stub', $configuration)) {
      $configuration['no_stub'] = TRUE;
    }
    $plugin = $manager->createInstance('migration_lookup', $configuration, $migration);
    $value = $plugin->transform($value, $migrate_executable, $row, $destination_property);
    return $value;
  }

}
