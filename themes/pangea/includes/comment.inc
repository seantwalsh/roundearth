<?php
/**
 * @file
 * Theme and preprocess functions for comments
 */

/**
 * Implementes template_preprocess_comment.
 */
function pangea_preprocess_comment(&$variables) {
  /** @var \Drupal\comment\CommentInterface $comment */
  $comment = $variables['comment'];

  // Format and add the date to template.
  $variables['date_formatted'] = \Drupal::service('date.formatter')->format($comment->getCreatedTime(), 'custom', 'F d, Y');

  // Add the permalink url to the template.
  if (!$comment->isNew()) {
    $variables['permalink_url'] = $comment->permalink()->toString();
  }
}
