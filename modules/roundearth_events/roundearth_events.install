<?php
/**
 * @file
 * Install hooks for Roundearth: Events.
 */

/**
 * Implements hook_install().
 */
function roundearth_events_install() {
  _roundearth_events_install_civicrm_attendee_fields();
}

/**
 * Helper function for creating the custom attendee fields.
 */
function _roundearth_events_install_civicrm_attendee_fields() {
  \Drupal::service('civicrm')->initialize();

  $result = civicrm_api3('CustomGroup', 'get', ['name' => 'roundearth_events_info']);
  if (!empty($result['values'])) {
    $group = reset($result['values']);
  }
  else {
    $result = civicrm_api3('CustomGroup', 'create', array(
      'title' => 'Attendee info',
      'name' => 'roundearth_events_info',
      'extends' => "Individual",
      'style' => 'Inline',
      'collapse_display' => 1,
      'help_post' => 'Information stored about event attendees, which could pontentially be shown publicly, for example, in a speaker bio.',
      'weight' => 2,
      'is_active' => 1,
      'is_public' => 1,
    ));
    $group = reset($result['values']);
  }

  $result = civicrm_api3('CustomField', 'get', ['name' => 'roundearth_events_bio']);
  if (empty($result['values'])) {
    $result = civicrm_api3('CustomField', 'create', [
      'label' => 'Attendee bio',
      'custom_group_id' => 'roundearth_events_info',
      'name' => 'roundearth_events_bio',
      'data_type' => 'Memo',
      'html_type' => 'RichTextEditor',
      'weight' => 1,
    ]);
    $bio_field = reset($result['values']);
  }
}

/**
 * Enable civicrm_entity and paragraphs.
 */
function roundearth_events_update_8001() {
  /** @var \Drupal\Core\Extension\ModuleInstallerInterface $module_installer */
  $module_installer = \Drupal::service('module_installer');
  $module_installer->install(['civicrm_entity', 'paragraphs']);

  // Enable CiviCRM event entities.
  $config = \Drupal::configFactory()->getEditable('civicrm_entity.settings');
  $config->set('filter_format', 'panopoly_wysiwyg_basic');
  $enabled_entity_types = $config->get('enabled_entity_types');
  $enabled_entity_types['civicrm_event'] = 'civicrm_event';
  $enabled_entity_types['civicrm_contact'] = 'civicrm_contact';
  $config->set('enabled_entity_types', $enabled_entity_types);
  $config->save();

  // Clear entity type definitions.
  \Drupal::entityTypeManager()->clearCachedDefinitions();

  // Re-import the upcoming events view
  /** @var \Drupal\config_update\ConfigReverter $config_update */
  $config_update = \Drupal::service('config_update.config_update');
  $config_update->revert('view', 'roundearth_events_upcoming');

  // Disable the civicrm_views entity.
  $module_installer->uninstall(['civicrm_views']);
}

/**
 * Add custom field for attendee bios.
 */
function roundearth_events_update_8002() {
  _roundearth_events_install_civicrm_attendee_fields();
}

/**
 * Add a custom event_date_medium format for event dates.
 */
function roundearth_events_update_8003() {
  $storage = \Drupal::entityTypeManager()->getStorage('date_format');
  $storage->create([
    'id' => 'event_date_medium',
    'label' => 'Event date medium',
    'pattern' => 'F d Y - H:i',
  ])->save();
}

/**
 * Enable the events admin view.
 */
function roundearth_events_update_8004() {
  if (!\Drupal\views\Views::getView('roundearth_events_admin')) {
    // Import the roundearth_events_admin view.
    $config_update = \Drupal::service('config_update.config_update');
    $config_update->import('view', 'roundearth_events_admin');
  }
}
