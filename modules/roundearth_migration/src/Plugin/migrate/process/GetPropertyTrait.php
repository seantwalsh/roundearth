<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\Plugin\migrate\process;
use Drupal\migrate\Row;

trait GetPropertyTrait {

  /**
   * Uses the 'get' process plugin to get a property from the row.
   *
   * @param mixed $value
   *   The value to be transformed.
   * @param \Drupal\migrate\MigrateExecutableInterface $migrate_executable
   *   The migration in which this process is being executed.
   * @param \Drupal\migrate\Row $row
   *   The row from the source to process. Normally, just transforming the value
   *   is adequate but very rarely you might need to change two columns at the
   *   same time or something like that.
   * @param string $destination_property
   *   The destination property currently worked on. This is only used together
   *   with the $row above.
   *
   * @return mixed|null
   *   The property value.
   *
   * @see \Drupal\migrate\Plugin\MigrateProcessInterface::transform.
   */
  protected function getProperty($value, $migrate_executable, $row, $destination_property) {
    /** @var \Drupal\migrate\Plugin\MigratePluginManager $manager */
    $manager = \Drupal::service('plugin.manager.migrate.process');
    $plugin = $manager->createInstance('get', ['source' => $value]);
    $value = $plugin->transform($value, $migrate_executable, $row, $destination_property);
    return $value;
  }
}
