<?php
/**
 * @file
 * Hook implementations for Roundearth: Events module.
 */

use Drupal\civicrm_entity\Entity\CivicrmEntity;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Implements hook_entity_base_field_info().
 */
function roundearth_events_entity_base_field_info(EntityTypeInterface $entity_type) {
  if ($entity_type->id() === 'civicrm_event') {
    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setComputed(TRUE);

    return $fields;
  }
}

/**
 * Implements hook_entity_base_field_info_alter().
 */
function roundearth_events_entity_base_field_info_alter(&$fields, EntityTypeInterface $entity_type) {
  if ($entity_type->id() === 'civicrm_event' && isset($fields['description'])) {
    $fields['description']->setDescription(t('Free-form description of event. Displayed at the top of the "About" section. Use "Event info sections" for more structured information.'));
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function roundearth_events_preprocess_node_add_list(&$vars) {
  $vars['types']['event'] = [
    'type' => 'roundearth_events-civicrm_event',
    'add_link' => Link::createFromRoute(t('Event'), 'entity.civicrm_event.add_form'),
    'description' => [
      '#markup' => 'Use an <em>Event</em> to add a meeting, workshop, seminar, or conference any other type of event.',
    ],
    // For compatibility with the Seven template.
    'label' => t('Event'),
    'url' => new Url('entity.civicrm_event.add_form'),
  ];

  // @todo This should really sort by display name, but in the theme
  ksort($vars['types']);
}


/**
 * Return an array of share links for an event.
 *
 * @param \Drupal\Core\Entity\EntityInterface $event
 *
 * @return array
 */
function roundearth_events_get_social_media_links_for_event(EntityInterface $event) {
  $links = [
    'facebook' => [
      'icon' => 'facebook',
      'template' => 'https://www.facebook.com/sharer/sharer.php?u={{ url }}',
    ],
    'twitter' => [
      'icon' => 'twitter',
      'template' => 'https://twitter.com/home?status={{ title }} {{ url }}'
    ],
    'linkedin' => [
      'icon' => 'linkedin',
      'template' => 'https://www.linkedin.com/shareArticle?mini=true&url={{ url }}&title={{ title }}',
    ],
    'google_plus' => [
      'icon' => 'google-plus',
      'template' => 'https://plus.google.com/share?url={{ url }}',
    ],
  ];

  try {
    $context = [
      'title' => $event->label(),
      'url' => $event->toUrl('canonical', ['absolute' => TRUE]),
    ];

    foreach ($links as $name => $network) {
      $links[$name]['url'] = [
        '#type' => 'inline_template',
        '#template' => $network['template'],
        '#context' => $context,
      ];
    }
  }
  catch (EntityMalformedException $exception) {
    Drupal::logger('civicrm')->error($exception->getMessage(), 'civicrm');
  }

  return $links;
}

/**
 * Returns the address for an event.
 *
 * @param \Drupal\Core\Entity\EntityInterface $event
 *
 * @param bool $format
 *   Set to TRUE if address should be formatted using CiviCRM format.
 *
 * @return array
 */
function roundearth_events_address_for_event(EntityInterface $event, $format = FALSE) {
  // Get the location.
  if (($event->hasField('loc_block_id')) && ($location_id = $event->get('loc_block_id')->target_id)) {
    try {
      $locations = civicrm_api3('LocBlock', 'get', [
        'id' => $location_id,
        'return' => 'all',
      ]);

      if ($locations['count']) {
        $location = reset($locations['values']);
        $address = $location['address'];

        // Add the country name to address.
        if (isset($address['country_id'])) {
          try {
            $countries = civicrm_api3('Country', 'get', [
              'id' => $address['country_id'],
            ]);

            if ($countries['count']) {
              $country = reset($countries['values']);
              if (isset($country['name'])) {
                $address['country'] = $country['name'];
              }
            }
          }
          catch (CiviCRM_API3_Exception $e) {
            Drupal::logger('civicrm')->error($e->getMessage(), 'civicrm');
          }
        }

        if ($format) {
          return CRM_Utils_Address::format($address);
        }

        return $address;
      }
    }
    catch (CiviCRM_API3_Exception $e) {
      Drupal::logger('civicrm')->error($e->getMessage(), 'civicrm');
    }
  }

  return [];
}

/**
 * Returns an array of prices with tax for an event.
 *
 * @param \Drupal\Core\Entity\EntityInterface $event
 *
 * @return array
 */
function roundearth_events_get_prices_for_event(EntityInterface $event) {
  if ($price_set_id = Drupal::database()->query('SELECT price_set_id FROM {civicrm_price_set_entity} WHERE entity_id = :entity_id AND entity_table = :entity_type', [
    'entity_id' => $event->id(),
    'entity_type' => $event->getEntityTypeId(),
  ])->fetchField(0)) {

    try{
      /// Get the prices fields for this set.
      $price_fields = civicrm_api3('PriceField', 'get', [
        'price_set_id' => $price_set_id,
      ]);

      if ($price_fields['count']) {
        $price_field = reset($price_fields['values']);

        try {
          $price_field_values = civicrm_api3('PriceFieldValue', 'get', [
            'price_field_id' => $price_field['id'],
          ]);

          if ($price_field_values['count']) {
            // Get tax settings.
            $invoice_settings = Civi::settings()->get('contribution_invoice_settings');
            $tax_term = CRM_Utils_Array::value('tax_term', $invoice_settings);
            $tax_rates = CRM_Core_PseudoConstant::getTaxRates();

            foreach ($price_field_values['values'] as $key => $price_field_value) {
              // Add formatted prices.
              $price_field_values['values'][$key]['price_formatted'] = CRM_Utils_Money::format($price_field_value['amount']);

              // Add tax.
              if (isset($tax_rates[$price_field_value['financial_type_id']])) {
                $tax = CRM_Contribute_BAO_Contribution_Utils::calculateTaxAmount($price_field_value['amount'], $tax_rates[$price_field_value['financial_type_id']]);
                $price_field_values['values'][$key]['tax_amount'] = CRM_Utils_Money::format($tax['tax_amount']);
                $price_field_values['values'][$key]['tax_term'] = $tax_term;
              }
            }

            return $price_field_values['values'];
          }
        }
        catch (CiviCRM_API3_Exception $e) {
          Drupal::logger('civicrm')->error($e->getMessage(), 'civicrm');
        }

      }
    }
    catch (CiviCRM_API3_Exception $e) {
      Drupal::logger('civicrm')->error($e->getMessage(), 'civicrm');
    }
  }

  return [];
}

/**
 * Helper to get a custom field value from an entity based on custom field name.
 *
 * @param $custom_field_name
 *
 * @param \Drupal\civicrm_entity\Entity\CivicrmEntity $entity
 *
 * @return mixed|null
 */
function roundearth_events_get_custom_field_value_for_entity($custom_field_name, CivicrmEntity $entity) {
  try {
    $results = civicrm_api3('CustomField', 'get', [
      'sequential' => 1,
      'name' => $custom_field_name,
    ]);

    // Find the custom group id.
    // From this we can find the custom table name.
    if ($results['count']) {
      $field = reset($results['values']);

      if (isset($field['custom_group_id']) && isset($field['column_name'])) {
        try {
          $groups = civicrm_api3('CustomGroup', 'get', [
            'sequential' => 1,
            'id' => $field['custom_group_id'],
          ]);

          if ($groups['count']) {
            $group = reset($groups['values']);

            // Query the custom column from the custom table.
            if (isset($group['table_name'])) {
              if($value = Drupal::database()->select($group['table_name'], 'tn')
                ->fields('tn', [$field['column_name']])
                ->condition('entity_id', $entity->id())
                ->execute()
                ->fetchField(0)) {
                return $value;
              }
            }
          }
        }
        catch (CiviCRM_API3_Exception $e) {
          Drupal::logger('civicrm')->error($e->getMessage(), 'civicrm');
        }
      }
    }
  }
  catch (CiviCRM_API3_Exception $e) {
    Drupal::logger('civicrm')->error($e->getMessage(), 'civicrm');
  }

  return NULL;
}

/**
 * Implements hook_civicrm_permission_check().
 */
function roundearth_events_civicrm_permission_check($permission, &$granted) {
    if ($permission == 'edit all events') {
        $param = \Drupal::request()->query->all();
        $event_id = isset($param['id'])? $param['id'] : null;

        $edit_own_events = \Drupal::currentUser()->hasPermission('edit own events');

        if (!is_null($event_id) && $edit_own_events) {
            \Drupal::service('civicrm')->initialize();

            try {
              $event_entity = civicrm_api3('Event', 'getsingle', [
                  'id' => $event_id,
              ]);

              $user_id = \Drupal::currentUser()->id();

              $contact = civicrm_api3('User', 'getsingle', [
                  'id' => $user_id,
              ]);

              if (!empty($event_entity) && !empty($contact) && $contact["is_error"] != 1) {
                if ($event_entity['created_id'] == $contact['contact_id']) {
                  $granted = TRUE;
                }
              }
            }
            catch (CiviCRM_API3_Exception $e) {
              $errorMessage = $e->getMessage();
              \Drupal::logger('roundearth_events')->error($errorMessage);
              $contact = NULL;
            }
        }
    }
}
