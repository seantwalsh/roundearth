<?php

/**
 * @file
 * Hooks for the Roundearth Gallery feature.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\Core\Link;
use \Drupal\Core\Entity\EntityInterface;
use \Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * Implements hook_preprocess_node().
 */
function roundearth_gallery_preprocess_node(&$vars) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $vars['node'];
  if ($node->getType() === 'roundearth_gallery') {
    if ($vars['view_mode'] === 'teaser') {
      // Only render the first image in the gallery.
      foreach (Element::children($vars['content']['field_roundearth_gallery_media']) as $name) {
        if (is_numeric($name) && $name > 0) {
          unset($vars['content']['field_roundearth_gallery_media'][$name]);
        }
      }
    }

    // Replace links to taxonomy terms with links to view.
    foreach (Element::children($vars['content']['field_roundearth_gallery_tags']) as $name) {
      $link =& $vars['content']['field_roundearth_gallery_tags'][$name];
      $link['#url'] = Url::fromRoute('view.roundearth_gallery.tags', [
        'arg_0' => $link['#url']->getOption('entity')->id(),
      ]);
    }
  }
}

/**
 * Implements hook_preprocess_media().
 */
function roundearth_gallery_preprocess_media(&$vars) {
  /** @var \Drupal\media\MediaInterface $media */
  $media = $vars['media'];
  if ($media->bundle() === 'image' && $vars['view_mode'] === 'roundearth_gallery') {
    $vars['attributes']['class'][] = 'roundearth-gallery-image';

    // Replace links to taxonomy terms with links to view.
    foreach (Element::children($vars['content']['field_panopoly_media_tags']) as $name) {
      $link =& $vars['content']['field_panopoly_media_tags'][$name];
      $link['#url'] = Url::fromRoute('view.roundearth_gallery_media.tags', [
        'arg_0' => $link['#url']->getOption('entity')->id(),
      ]);
    }
  }
}

/**
 * Implements hook_entity_extra_field_info().
 */
function roundearth_gallery_entity_extra_field_info() {
  $extra = array();
  $extra['node']['roundearth_gallery']['display']['link_to_gallery'] = array(
    'label' => t('Link to gallery'),
    'description' => t('Link to gallery view'),
    'weight' => -1,
    'visible' => FALSE,
  );
  return $extra;
}

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function roundearth_gallery_node_view(array &$build, EntityInterface $entity,
  EntityViewDisplayInterface $display, $view_mode) {
    if ($display->getComponent('link_to_gallery')) {
      $build['link_to_gallery'] = [
        '#type' => 'link',
        '#title' => t("Back to gallery listing"),
        '#url' => Url::fromRoute('view.roundearth_gallery.gallery'),
      ];
    }
}
