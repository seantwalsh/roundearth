<?php

namespace Drupal\roundearth_events;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides dynamic permissions for nodes of different types.
 */
class CiviCRMPermissions {

    use StringTranslationTrait;

    /**
     * Returns an array of node type permissions.
     *
     * @return array
     *   The node type permissions.
     *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
     */
    public function getPermissions() {
        $permissions = [
            'edit own events' => [
                'title' => $this->t("Edit own events"),
                'description' => $this->t("Edit own CiviCRM Events"),
            ]
        ];

        return $permissions;
    }

}
