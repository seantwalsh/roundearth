<?php

namespace Drupal\roundearth_migration\Plugin\migrate\source\d6;

use Drupal\node\Plugin\migrate\source\d6\Node as NodeD6;

/**
 * Node source plugin that generates a new row for each value of a field.
 *
 * @MigrateSource(
 *   id = "d6_node_field",
 *   source_module = "node"
 * )
 */
class NodeField extends NodeD6 {

  /**
   * Gets the node type from configuration.
   *
   * @return string
   */
  protected function getNodeType() {
    return $this->configuration['node_type'];
  }

  /**
   * Gets the field name from configuration.
   *
   * @return string
   */
  protected function getFieldName() {
    return $this->configuration['field_name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids = parent::getIds();
    $ids['delta'] = [
      'type' => 'integer',
      'alias' => 'fd',
    ];
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'delta' => $this->t('Field delta'),
    ] + parent::fields();
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    $node_type = $this->getNodeType();
    $field_name = $this->getFieldName();

    $field_info = $this->getFieldInfo($node_type);

    $field_table = 'content_' . $field_name;
    $node_table = 'content_type_' . $node_type;

    /** @var \Drupal\Core\Database\Schema $db */
    $db = $this->getDatabase()->schema();

    if ($db->tableExists($field_table)) {
      $query->leftJoin($field_table, 'fd', 'n.nid = fd.nid AND n.vid = fd.vid');
      $query->addField('fd', 'delta');
    }
    elseif ($db->tableExists($node_table)) {
      // @todo: I haven't had one of these to test with yet.
    }

    $columns = array_keys($field_info[$field_name]['db_columns']);
    foreach ($columns as $column) {
      $query->addField('fd', $field_name . '_' . $column, 'field_' . $column);
    }

    $query->orderBy('n.nid');
    $query->orderBy('fd.delta');

    return $query;
  }

}