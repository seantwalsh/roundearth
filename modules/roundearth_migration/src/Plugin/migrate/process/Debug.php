<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Class Debug.
 *
 * Configuration options:
 *
 * - source_data: Set to debug the source data, by default the destination will
 *   be debugged.
 * - property: The property os the source/destination to debug, if empty the
 *   entire source/destination will be debugged.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_debug"
 * )
 */
class Debug extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($this->configuration['source_data'])) {
      $this->debugDest($row);
    }
    else {
      $this->debugSource($row);
    }
    throw new MigrateException();
  }

  /**
   * Debug a value.
   *
   * @param mixed $value
   *   The value to debug.
   */
  protected function debug($value) {
    // Could switch this out via config to send to log, breakpoint, other.
    var_dump($value);
  }

  /**
   * Debug the row destination.
   *
   * @param \Drupal\migrate\Row $row
   *   The row.
   */
  protected function debugDest(Row $row) {
    if (!empty($this->configuration['property'])) {
      // Specific property.
      $this->debug($row->getDestinationProperty($this->configuration['property']));
    }
    else {
      // The whole thing.
      $this->debug($row->getDestination());
    }
  }

  /**
   * Debug the row source.
   *
   * @param \Drupal\migrate\Row $row
   *   The row.
   */
  protected function debugSource(Row $row) {
    if (!empty($this->configuration['property'])) {
      // Specific property.
      $this->debug($row->getSourceProperty($this->configuration['property']));
    }
    else {
      // The whole thing.
      $this->debug($row->getSource());
    }
  }

}
