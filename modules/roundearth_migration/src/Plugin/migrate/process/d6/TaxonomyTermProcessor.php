<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process\d6;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Class TaxonomyTermProcessor.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_d6_term_processor",
 *   handle_multiples = TRUE
 * )
 */
class TaxonomyTermProcessor extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!$value) {
      return [];
    }

    $vid = $this->configuration['vid'];
    if (!is_array($vid)) {
      $vid = [$vid];
    }

    return $this->filterVid($value, $vid);
  }

  /**
   * Filter taxonomy term data to specified VIDs.
   *
   * @param array $values
   *   The source data.
   * @param array $vids
   *   The vocabulary IDs to filter for.
   *
   * @return array
   *   The filtered term data.
   */
  protected function filterVid(array $values, array $vids) {
    return array_filter($values, function ($item) use ($vids) {
      return in_array($item['vid'], $vids);
    });
  }

}
